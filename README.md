# timestamp-check

Program to check if timestamps in logfiles are monotonic.

Timestamp must be in ISO8601 format.
This is specifically made to detect errors in sensor data saved in a file.
These errors were cause by various things, and the file contained 1Mio lines,
and processing this with bash, perl or python just takes forever, therefore this 
was made in C.


example of such a data line:

2020-01-01T00:00:00,5.18,0.04,995.54,1034.17,69.43,4.77 0
