/*
  Monotonic timestamp filter
  
  Input file must have lines that all start with a timestamp in ISO8601.
  Subsequent data may follow the timestamps.
  Lines in a file with non-monotonic timestamps are filtered out.
  
  Timestamp frequency is expected to be 60 s , deviations 
  larger than tolerated create a warning, but no filtering.
  
  2022-10-04 J. Schwender
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#define __USE_XOPEN
#include <time.h>
//#define DEBUG
#define MAX_LINE_LENGTH 1023
const int Tmin = 40;
const int Tmax = 70;

void my_help(void) {
    printf("command line options:\n  -i input file\n");
    printf("  read file, the first data is an ISO8601 date. Tt is tested for monotony\n");
    printf("  creates an [input file].err for errors and and [input file].cleaned with monotinic timestamps.\n");
}

//########################################################################
int main(int argc, char* argv[])
{
	int i=0,LineCount=0;                          // loop counter
	char InputLine[MAX_LINE_LENGTH];
	int InFileIndex=0;
	FILE *InFile, *OutFile, *OutErrFile;
	struct tm tm1={0};   // timestamp
	double Tdiff;
	time_t t1=0, t2=0;
	if ( argc <= 1 ) { my_help(); exit(0); }
	for(i=1; i < argc; ++i)  {
	     if( !strcmp( argv[i], "--help" ) ) { my_help(); exit(0); }
	     if( !strcmp( argv[i], "-help" ) )  { my_help(); exit(0); }
	     if( !strcmp( argv[i], "-i" ) ) {
	            if ( (i+1) >= argc ) { my_help(); exit(1); }  // avoid overflow if incomplete option provided
		    InFileIndex = i+1;
	     }
	}
	char *OutFileName = strdup(argv[InFileIndex]);   // copy file name from arv structure
	char *OutErrFileName = strdup(argv[InFileIndex]);
	strcat(OutFileName, ".cleaned");   // same file name with .cleaned
	strcat(OutErrFileName, ".err");

	if ((InFile = fopen(argv[InFileIndex], "r")) == NULL) {
	        printf("Error! input file cannot be opened.\n");        // Program exits if the file pointer returns NULL.
	        exit(1);
	}
	if ((OutFile = fopen(OutFileName, "w")) == NULL) {
        	printf("Error! output file cannot be opened.\n");
	        exit(1);           // Program exits if the file pointer returns NULL.
    	}
	if ((OutErrFile = fopen(OutErrFileName, "w")) == NULL) {
        	printf("Error! output file cannot be opened.\n");
	        exit(1);           // Program exits if the file pointer returns NULL.
	}

	while (fscanf(InFile, "%[^\n] ", InputLine) != EOF) {
		LineCount++;
		strptime(InputLine, "%Y-%m-%dT%H:%M:%S", &tm1);  // turns the timestamp string into a time structure tm defined by time.h
		if ( LineCount == 1 )  {  // very first input line, there is no old value available and monotonie is undefined
			t2 = t1;   // store value to compare to next TS   
			fprintf(OutFile,"%s\n", InputLine);
			continue;
		}

		t1 = mktime(&tm1);    // turns the time value from the struct into a float number representing seconds
		Tdiff = difftime(t1, t2);

		if ( Tdiff <= 0 ) {
			printf("Z%i  non-monotone %s\n",LineCount, InputLine);
			fprintf(OutErrFile,"%i %s\n", LineCount, InputLine);
			continue;   // Ausgabe unterdrücken
		}

		if (( Tdiff <= Tmin ) || (Tdiff >= Tmax) ) {
			printf("Z%i unusual timestep %f %s\n", LineCount, Tdiff, InputLine);  // trotzdem ausgeben
		}
	
		fprintf(OutFile,"%s\n", InputLine);
		t2 = t1;   // store value to compare to next TS
	}   // while loop END

   fclose(OutFile);
   fclose(OutErrFile);
   fclose(InFile);
  return 0;
}



